package ru.tinkoff;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import reactor.core.publisher.Flux;

public class Main {

    private static final int REPEAT_COUNT = 5;

    public static void main(String[] args) {
        String fileName = "D:\\books.txt";
        Path path = Paths.get(fileName);
        System.out.println("=".repeat(10) + "Reactive version" + "=".repeat(10) + "\n");
        Metric reactiveMetric = calcMetric(Main::reactive, path);
        System.out.println("\n" + "=".repeat(12) + "List version" + "=".repeat(12) + "\n");
        Metric listMetric = calcMetric(Main::list, path);
        System.out.println("\nReactive time: " + reactiveMetric.time.toString() + ", List time: " + listMetric.time.toString());
        System.out.println("\nReactive memory: " + reactiveMetric.memory.toString() + ", List memory: " + listMetric.memory.toString());

    }


    private static void reactive(Path path) {
        try (Stream<String> lines = Files.lines(path)) {
            Flux.fromStream(lines).filter(s -> s.contains("Author:") || s.contains("Title:"))
                .map(String::trim).subscribe(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void list(Path path) {
        try (Stream<String> lines = Files.lines(path)) {
            lines
                .filter(s -> s.contains("Author:") || s.contains("Title:"))
                .map(String::trim)
                .forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Metric calcMetric(Consumer<Path> readFile, Path filePath) {
        Runtime runtime = Runtime.getRuntime();
        BigInteger avgTime = BigInteger.ZERO;
        BigInteger startTime = BigInteger.ZERO;
        BigInteger endTime = BigInteger.ZERO;
        BigInteger startMemory = BigInteger.ZERO;
        BigInteger endMemory = BigInteger.ZERO;
        BigInteger avgMemory = BigInteger.ZERO;
        for (int i = 0; i < REPEAT_COUNT; i++) {
            startTime = BigInteger.valueOf(System.nanoTime());
            startMemory = BigInteger.valueOf(runtime.freeMemory());

            readFile.accept(filePath);

            endTime = BigInteger.valueOf(System.nanoTime());
            endMemory =BigInteger.valueOf(runtime.freeMemory());
        }

        avgTime =avgTime.add(endTime.subtract(startTime));
        avgTime = avgTime.divide(BigInteger.valueOf(REPEAT_COUNT));

        avgMemory = avgMemory.add(startMemory.subtract(endMemory));
        avgMemory = avgMemory.divide(BigInteger.valueOf(REPEAT_COUNT));
        return new Metric(avgTime, avgMemory);

    }

    record Metric(BigInteger time, BigInteger memory) {

    }

}