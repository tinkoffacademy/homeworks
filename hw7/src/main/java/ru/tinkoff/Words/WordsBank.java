package ru.tinkoff.Words;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import ru.tinkoff.utils.RandomGenerator;
import ru.tinkoff.utils.WorldsLoader;

public class WordsBank {

    private static Map<Integer, List<String>> words; //{wordsLength, [words]}
    private static List<String> specialSymbols;
    private static List<Integer> wordsLength;

    static {
        LoadWorlds();
        LoadSpecialSymbols();
    }

    public static String generateWord(int wordLength) {
        return words.get(wordLength)
            .get(RandomGenerator.getRandomNumberInRange(words.get(wordLength).size()));
    }

    public static String generateSpecialSymbol() {
        return specialSymbols.get(RandomGenerator.getRandomNumberInRange(specialSymbols.size()));
    }

    public static List<Integer> getWordsLength() {
        return List.copyOf(wordsLength);
    }

    private static void LoadWorlds() {
        wordsLength = new ArrayList<>();
        Set<Integer> lengths = new HashSet<>();
        words = new HashMap<>();
        for (String fileName : WordBankConst.WORDS_FILE_NAMES) {
            Optional<List<String>> wordsList = WorldsLoader.load(fileName);
            wordsList.ifPresent(strings -> words.put(strings.get(0).length(), strings));
            wordsList.ifPresent(strings -> {
                lengths.add(strings.get(0).length());
            });
        }

        wordsLength.addAll(lengths);
        Collections.sort(wordsLength);
        Collections.reverse(wordsLength);
    }

    private static void LoadSpecialSymbols() {
        Optional<List<String>> specialSymbolsList = WorldsLoader.load(
            WordBankConst.SPECIAL_SYMBOLS_FILE_NAME);
        specialSymbolsList.ifPresent(strings -> specialSymbols = strings);
    }


}
