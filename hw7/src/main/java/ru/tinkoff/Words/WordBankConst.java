package ru.tinkoff.Words;

import java.util.List;

public class WordBankConst {

    public static final List<String> WORDS_FILE_NAMES = List.of("sevenLettersWords.txt",
        "sixLettersWords.txt"
        , "fiveLettersWords.txt"
        , "fourLettersWords.txt"
        , "threeLettersWords.txt"
        , "oneLetterWords.txt");

    public static final String SPECIAL_SYMBOLS_FILE_NAME = "specialSymbols.txt";

}
