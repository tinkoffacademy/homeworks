package ru.tinkoff;


import org.apache.commons.cli.ParseException;
import ru.tinkoff.PasswordGenerator.PasswordGenerator;
import ru.tinkoff.PasswordGenerator.PasswordGeneratorParams;
import ru.tinkoff.utils.ArgParser;

public class Main {

    public static void main(String[] args) {
        try {
            PasswordGeneratorParams params = ArgParser.parse(args);
            String password = PasswordGenerator.generate(params);
            System.out.println(password);

        } catch (ParseException e) {
            System.out.println(("Invalid arguments"));
        }


    }
}