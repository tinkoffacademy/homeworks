package ru.tinkoff.utils;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import ru.tinkoff.PasswordGenerator.PasswordGeneratorParams;

public class ArgParser {

    public static PasswordGeneratorParams parse(String[] args) throws ParseException {

        Option passwordLen = new Option("l", "password_length", true, "password length");
        Option isUseNumbers = new Option("n", "numbers", false, "use numbers in password");
        Option isUseSpecialSymbols = new Option("s", "specialSymbols", false,
            "use special symbols in password");

        Options options = new Options();
        options.addOption(passwordLen);
        options.addOption(isUseNumbers);
        options.addOption(isUseSpecialSymbols);

        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, args);

        int length = 20;
        if (line.hasOption("l")) {
            length = Integer.parseInt(line.getOptionValue("l"));
        }
        boolean useNumbers = false;
        if (line.hasOption("n")) {
            useNumbers = true;
        }
        boolean useSpecialSymbols = false;
        if (line.hasOption("s")) {
            useSpecialSymbols = true;
        }

        return new PasswordGeneratorParams(useSpecialSymbols, useNumbers, length);
    }
}
