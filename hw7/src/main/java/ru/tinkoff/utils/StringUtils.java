package ru.tinkoff.utils;

public class StringUtils {

    public static String toTitleCase(String s) {
        if (s.isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

}
