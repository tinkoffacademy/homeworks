package ru.tinkoff.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import javax.imageio.IIOException;

public class WorldsLoader {

    public static Optional<List<String>> load(String fileName)  {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        URL url = classloader.getResource(fileName);
        Path filePath;
        List<String> lines = null;
        try {
            filePath = Paths.get(url.toURI());
            lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
        }
        catch (URISyntaxException | IOException e) {
            System.out.println("File with name: " + fileName + " not found");
        }

        return Optional.ofNullable(lines);

    }

}
