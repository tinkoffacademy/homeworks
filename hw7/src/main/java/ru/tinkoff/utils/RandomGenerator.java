package ru.tinkoff.utils;


public class RandomGenerator {

    public static int getRandomNumberInRange(int maxValue) {
        return (int) (Math.random() * maxValue); // random number in [0, maxValue)
    }

}
