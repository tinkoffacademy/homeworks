package ru.tinkoff.PasswordGenerator;


import static ru.tinkoff.utils.StringUtils.toTitleCase;
import ru.tinkoff.Words.WordsBank;
import ru.tinkoff.utils.RandomGenerator;

public class PasswordGenerator {

    public static String generate(PasswordGeneratorParams param) {

        int countSpecialSymbols = 0;
        if (param.isUseSpecialSymbols()) {
            countSpecialSymbols =
                RandomGenerator.getRandomNumberInRange(GeneratorConst.MAX_COUNT_SPECIAL_SYMBOLS)
                    + 1;
        }
        int countNumbers = 0;
        if (param.isUseNumbers()) {
            countNumbers =
                RandomGenerator.getRandomNumberInRange(GeneratorConst.MAX_COUNT_NUMBERS) + 1;
        }

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < countNumbers; i++) {
            builder.append(RandomGenerator.getRandomNumberInRange(10));
        }
        int wordLength = param.passwordLength() - countNumbers - countSpecialSymbols;

        while (wordLength > 0) {

            for (int length : WordsBank.getWordsLength()) {
                if (wordLength >= length) {
                    builder.append(toTitleCase(WordsBank.generateWord(length)));
                    wordLength -= length;
                    break;
                }
            }

        }
        for (int i = 0; i < countSpecialSymbols; i++) {
            builder.append(WordsBank.generateSpecialSymbol());
        }

        return builder.toString();
    }

}

