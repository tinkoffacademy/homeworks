package ru.tinkoff.PasswordGenerator;

public record PasswordGeneratorParams(boolean isUseSpecialSymbols, boolean isUseNumbers,
                                      int passwordLength) {

}
